import Vue from 'vue'
import { BIconLock, BootstrapVueIcons } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'
import { BVToastPlugin } from 'bootstrap-vue'
Vue.use(BootstrapVueIcons)
Vue.use(BVToastPlugin)

Vue.mixin({
  data() {
    return {
      IsLoading: false,
      lstRole: [
        {
          key: '1',
          value: 'Manager'
        },
        {
          key: '2',
          value: 'Staff'
        },
        {
          key: '3',
          value: 'Customer'
        },
      ],
    }
  },
  computed: {
    IsEdit() {
      var url = this.$route.name
      console.log(url)
      if (url != null) {
        return url.split('/')[0].includes('update')
      } else {
        return ''
      }
    },
  },
  methods: {
    goTo(url) {
      this.$router.push(url)
    },
    getValidationState({ dirty, validated, valid = null }) {
      var result = dirty || validated ? valid : null
      return result
    },
    start() {
      this.IsLoading = true
    },
    done() {
      this.IsLoading = false
    },
    currency(number) {
      if (isNaN(number)) return ''
      if (number == null) return ''
      var value = number.toLocaleString('vi-VN', {
        style: 'currency',
        currency: 'VND',
      })
      return value
    },
    datetimeToString(datetime) {
      var a = datetime
      var year = datetime.slice(0, 4)
      var month = datetime.slice(5, 7)
      var day = datetime.slice(8, 10)
      var hour = datetime.slice(11, 13)
      var minutes = datetime.slice(14, 16)
      var seconds = datetime.slice(17, 19)
      console.log(day);
      return `${day}-${month}-${year} | ${hour}:${minutes}:${seconds}`
    },
    getTimeWithoutDate(datetime) {
      // YYYY-MM-DDTHH:MM:SS
      console.log(datetime);
      datetime = datetime.toString()
      // var datetime = new Date(datetime.toString()).toISOString()
      console.log(datetime);

      // var year = datetime.slice(0, 4)
      // var month = datetime.slice(5, 7)
      // var day = datetime.slice(8, 10)
      var hour = datetime.slice(16, 18)
      var minutes = datetime.slice(19, 21)

      return `${hour}:${minutes}`
    },
    ///
    /// TOAST
    ///
    info(msg, title) {
      this.$bvToast.toast(msg || 'Không có nội dung', {
        title: title || 'Thông báo',
        autoHideDelay: 5000,
        appendToast: true,
        variant: 'info',
      })
    },
    success(msg, title) {
      this.$bvToast.toast(msg || 'Tác vụ đã thực hiện thành công', {
        title: title || 'Tác vụ thành công',
        autoHideDelay: 5000,
        appendToast: true,
        variant: 'success',
      })
    },
    danger(msg, title) {
      this.$bvToast.toast(msg || 'Rất tiếc đã có lỗi xảy ra', {
        title: title || 'Có lỗi xảy ra',
        autoHideDelay: 5000,
        appendToast: true,
        variant: 'danger',
      })
    },

    ///
    /// MODAL
    ///
    async confirmDeletion(msg) {
      return this.$bvModal.msgBoxConfirm(
        msg || 'Bạn có chắc là muốn xóa dữ liệu?',
        {
          title: 'Xác nhận xóa',
          size: 'sm',
          buttonSize: 'sm',
          okVariant: 'danger',
          okTitle: 'Xác nhận',
          cancelTitle: 'Không',
          footerClass: 'p-2',
          hideHeaderClose: false,
          centered: true,
        }
      )
    },
    async confirmAction(msg) {
      return this.$bvModal.msgBoxConfirm(
        msg || 'Bạn có chắc là muốn xóa thực hiện thao tác này?',
        {
          title: 'Xác nhận',
          size: 'sm',
          buttonSize: 'sm',
          okVariant: 'success',
          bodyTextVariant: 'danger',
          okTitle: 'Xác nhận',
          cancelTitle: 'Hủy',
          footerClass: 'p-2',
          hideHeaderClose: false,
          centered: true,
        }
      )
    },
    ///
    /// DOWNLOAD FILE
    ///
    async downloadFile(url, fileName) {
      try {
        var response = await this.$axios.$post(url, { responseType: 'blob' })

        const blob = new Blob([response.data], {
          type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        })
        console.log(response)
        const link = document.createElement('a')
        link.href = URL.createObjectURL(blob)
        link.download = fileName
        link.click()
        URL.revokeObjectURL(link.href)
        this.success('Tệp tin đã được tải về thành công')
      } catch (error) {
        this.catch(error)
      }
    },
    ///
    /// Upload Image
    ///
    async UploadMedia(file, code, category, isEdit, oldFilename = '') {
      if (file != null) {
        let formData = new FormData()
        formData.append('files', file)
        formData.append('code', code)
        formData.append('category', category)
        formData.append('isEdit', isEdit)
        formData.append('oldName', oldFilename)
        const response = await this.$axios.$post('Media/Upload', formData, {
          headers: {
            'Content-Type': `multipart/form-data`,
          },
        })
        if (response.isSuccess) {
          return response.message
        } else {
          return ''
        }
      }
    },
    preview(lstImg, index) {
      this.$imagePreview({
        initIndex: 0,
        images: lstImg,
        onClose: () => {
          console.log('close')
        },
      })
    },
    codeRandom(type) {
      return `${type}${Math.floor(Math.random() * 1000) + 1}`
    }
  },
})
