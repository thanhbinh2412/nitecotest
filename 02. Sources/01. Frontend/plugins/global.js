import Vue from 'vue'

// Internal Components
import CreateTable from '@/components/CreateTable'
import SummaryItem from '@/components/SummaryItem'
import AddFile from '@/components/AddFile'
import Fieldset from '@/components/FieldsetCus'

// External Lib
import * as VueGoogleMaps from 'vue2-google-maps'
import imagePreview from 'image-preview-vue'
import 'image-preview-vue/lib/imagepreviewvue.css'
import datePicker from 'vue-bootstrap-datetimepicker';

const components = { CreateTable, SummaryItem, AddFile, Fieldset, datePicker }

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyD1zkOvgAQDZmHYiljlfKLUh3k_tIGtAdU',
    libraries: 'places',
  },
})
Vue.use(imagePreview)
