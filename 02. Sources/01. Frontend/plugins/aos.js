import Vue from 'vue'
import AOS from 'aos'
import 'aos/dist/aos.css'

export default ({ app }, inject) => {
  app.AOS = new AOS.init({
    disable: window.innerWidth < 0,
    // offset: 200,
    duration: 600,
    easing: 'ease-in-out-cubic',
    once: false,
  })
}
