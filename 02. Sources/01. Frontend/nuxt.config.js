export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/logo.ico',
      },
      {
        rel: 'stylesheet',
        href: 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Be Vietnam',
      },
    ],
  },

  env: {
    baseUrl: process.env.BASE_URL || 'https://abtweb-b7409.firebaseapp.com',
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/colors.scss',
    'aos/dist/aos.css',
    '@/assets/css/material-dashboard.css',
  ],

  render: {
    resourceHints: false,
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '~/plugins/aos',
      ssr: false,
    },
    {
      src: '~/plugins/mixin',
      ssr: false,
    },
    {
      src: '~/plugins/axios',
      ssr: false,
    },
    {
      src: '~/plugins/global',
      ssr: false,
    },
    {
      src: '~/plugins/charts',
      ssr: false,
    },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/style-resources',
  ],

  /*
   ** Axios module configuration
   */
  axios: {
    baseURL: process.env.API_URL || 'http://localhost:31370/api/',
    debug: process.env.DEBUG || false,
    proxyHeaders: false,
    credentials: false,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    vendor: ['aos'],
  },

  styleResources: {
    scss: ['@/assets/scss/*.scss'],
  },
}
