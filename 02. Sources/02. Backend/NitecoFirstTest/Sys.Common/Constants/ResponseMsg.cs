﻿namespace Sys.Common.Constants
{
    public static class ResponseMsg
    {
        public const String INSERT_SUCCESS = "Insert Success";
        public const String UPDATE_SUCCESS = "Update Success";
        public const String INSERT_ERROR = "Insert Error";
        public const String UPDATE_ERROR = "Update Error";
    }
}
