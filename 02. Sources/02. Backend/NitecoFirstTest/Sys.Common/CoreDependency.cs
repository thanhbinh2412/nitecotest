﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Sys.Common.JWT;
using Sys.Common.Models;
using Sys.Common.Swagger;

namespace Sys.Common
{
    public static class CoreDependency
    {
        public static void InjectDependencies(IServiceCollection services)
        {
            #region Swagger
            SwaggerConfig.AddConfig(services);
            #endregion

            #region JWT
            #endregion JWT
        }

        public static void Configure(ListServices listService)
        {
            #region Swagger
            SwaggerConfig.AddUI(listService.App);

            listService.App.UseMiddleware<JwtMiddlewareTest>();
            listService.App.UseMiddleware<JwtMiddleware>();
            #endregion
        }
    }
}
