﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Common.Models
{
    public class Result<T>
    {
        public IList<string> Messages { get; set; }
        public T Data { get; set; }
        public bool Success { get; set; }
        public string StrackTrace { get; set; }
        public int TotalCount { get; set; }

        public Result()
        {
            Messages = new List<string>();
        }
    }

    public class MetaData
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }

        public bool HasNext
        {

            get { return PageSize * (CurrentPage - 1) < TotalCount; }
        }

        public bool HasPrevious
        {
            get { return PageSize * (CurrentPage - 1) > 0; }
        }

    }
}
