﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Common.Models
{
    public partial class ListServices
    {
        public IApplicationBuilder App { get; set; }
        public IWebHostEnvironment Env { get; set; }
    }
}
