﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Common.JWT
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;

        public JwtMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IJwtUtils jwtUtils)
        {
            //if (context.Request.Path.Value != "/api/User/Insert" && context.Request.Path.Value != "/api/Auth/Login")
            //{
            //    var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            //    var obj = jwtUtils.ValidateJwtToken(token);
            //    if (obj == null)
            //    {
            //        context.Response.StatusCode = StatusCodes.Status400BadRequest;
            //        return;
            //    }
            //    context.Items["obj"] = obj;
            //}

            await _next(context);
        }
    }
}
