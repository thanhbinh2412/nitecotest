﻿using Microsoft.AspNetCore.Http;

namespace Sys.Common.JWT
{
    public class JwtMiddlewareTest
    {
        private readonly RequestDelegate _next;

        public JwtMiddlewareTest(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            await _next(context);
        }
    }
}
