﻿using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Sys.Common.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Sys.Common.JWT
{
    public class JwtUtils : IJwtUtils
    {
        private readonly ILogger<JwtUtils> _logger;
        public JwtUtils(ILogger<JwtUtils> logger)
        {
            _logger = logger;
        }

        public string GenerateJwtToken(Auth user, string Id, string secretKey = "niteco")
        {
            // generate token that is valid for 15 minutes
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SystemConfig.JWT_KEY);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("username", user.UserName.ToString()), new Claim("UserCode", Id), new Claim("SecretKey", secretKey) }),
                Expires = DateTime.UtcNow.AddMinutes(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public string GenerateRefreshToken(Auth user, out DateTime Expire)
        {
            throw new NotImplementedException();
        }

        public RefreshToken GenerateRefreshToken(string ipAddress)
        {
            throw new NotImplementedException();
        }

        public bool ValidateJwtPrincipalSecretKey(string token)
        {
            throw new NotImplementedException();
        }

        public JWTResponse? ValidateJwtToken(string token)
        {
            if (token == null)
                return null;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SystemConfig.JWT_KEY);
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;

                var userId = jwtToken.Claims.FirstOrDefault(x => x.Type == "UserCode")?.Value?.ToString();
                var phonenumber = jwtToken.Claims.FirstOrDefault(x => x.Type == "phonenumber")?.Value?.ToString();
                var secretKey = jwtToken.Claims.FirstOrDefault(x => x.Type == "SecretKey")?.Value?.ToString();
                var userName = jwtToken.Claims.FirstOrDefault(x => x.Type == "username")?.Value?.ToString();
                var emp = jwtToken.Claims.FirstOrDefault(x => x.Type == "EmpId")?.Value?.ToString();

                JWTResponse response = new JWTResponse()
                {
                    PhoneNumber = phonenumber,
                    UserId = userId,
                    SecretKey = secretKey,
                    UserName = userName,
                    ErrorCode = null,
                    EmpId = emp
                };
                return response;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return null;
            }
        }
    }
}
