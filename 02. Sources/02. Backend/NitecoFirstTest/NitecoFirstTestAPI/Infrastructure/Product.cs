﻿using NitecoFirstTestAPI.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace NitecoFirstTestAPI.Infrastructure
{
    public class Product : BaseModel
    {
        public string? Name { get; set; }
        public decimal? Price { get; set; }
        public string? Description { get; set; }
        public int? Quatity { get; set; }

        [ForeignKey("Category")]
        public Guid? CategoryId { get; set; }
        public Category Category { get; set; }

    }
}
