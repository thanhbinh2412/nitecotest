﻿using NitecoFirstTestAPI.Models;

namespace NitecoFirstTestAPI.Infrastructure
{
    public class Category : BaseModel
    {
        public string? CategoryName { get; set; }
        public string? Description { get; set; }

        public ICollection<Product> Products { get; set; }

    }
}
