﻿using NitecoFirstTestAPI.Models;

namespace NitecoFirstTestAPI.Infrastructure
{
    public class Order : BaseModel
    {
        public Guid? Customer { get; set; }
        public Guid? Product { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? OrderDate { get; set; }
    }
}
