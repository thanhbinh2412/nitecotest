﻿using NitecoFirstTestAPI.Models;

namespace NitecoFirstTestAPI.Infrastructure
{
    public class Customer : BaseModel
    {
        public string? Name { get; set; }
        public string? Address { get; set; }

    }
}
