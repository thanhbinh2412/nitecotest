﻿using NitecoFirstTestAPI.Infrastructure;
using NitecoFirstTestAPI.Models;
using Sys.Common.Models;

namespace NitecoFirstTestAPI.Models
{
    public class Order : BaseModel
    {
        public string? Name { get; set; }
        public Guid? Customer { get; set; }
        public Guid? Product { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? OrderDate { get; set; }
    }

    public class OrderListResponse
    {
        public List<ProductListResponseItem> Items { get; set; }
        public MetaData Metadata { get; set; }
    }

    public class OrderListResponseItem
    {
        public Guid Id { get; set; }
        public Customer Customer { get; set; }
        public Product? Product { get; set; }
        public Category? Category { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? OrderDate { get; set; }
    }

    public class RequestOrder
    {
        public string? Name { get; set; }
        public Guid? Customer { get; set; }
        public Guid? Product { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? OrderDate { get; set; }
    }
}
