﻿namespace NitecoFirstTestAPI.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public bool is_deleted { get; set; } = false;
        public string created_by { get; set; }
        public DateTime created_date { get; set; }
        public string? updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
}
