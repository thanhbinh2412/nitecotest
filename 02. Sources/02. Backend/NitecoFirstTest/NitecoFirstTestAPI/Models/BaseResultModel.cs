﻿namespace DoftPosAPI.Models
{
    public interface IResponseModel
    {

    }
    public class BaseResultModel : IResponseModel
    {
        public int ObjectId { get; set; } = 0;
        public Guid ObjectGuidId { get; set; }
        public string Message { get; set; } = "";
        public bool IsSuccess { get; set; } = true;
        public object Data { get; set; }
    }

    public class ResultModelWithObject<T>
    {
        public int ObjectId { get; set; } = 0;
        public Guid ObjectGuidId { get; set; }
        public string Message { get; set; } = "";
        public bool IsSuccess { get; set; } = true;
        public T Data { get; set; }
    }
}
