﻿using NitecoFirstTestAPI.Models;
using Sys.Common.Models;

namespace NitecoFirstTestAPI.Models
{
    public class Product : BaseModel
    {
        public string? Name { get; set; }
        public Guid? Category { get; set; }
        public decimal? Price { get; set; }
        public string? Description { get; set; }
        public int? Quatity { get; set; }
    }

    public class ProductListResponse
    {
        public List<ProductListResponseItem> Items { get; set; }
        public MetaData Metadata { get; set; }
    }

    public class ProductListResponseItem
    {

    }
}
