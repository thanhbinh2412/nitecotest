﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NitecoFirstTestAPI.Migrations
{
    /// <inheritdoc />
    public partial class AddFK2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryName",
                table: "Products");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CategoryName",
                table: "Products",
                type: "text",
                nullable: true);
        }
    }
}
