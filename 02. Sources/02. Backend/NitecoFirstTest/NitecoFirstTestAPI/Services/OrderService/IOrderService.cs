﻿using DoftPosAPI.Models;
using NitecoFirstTestAPI.Models;

namespace NitecoFirstTestAPI.Services.OrderService
{
    public interface IOrderService
    {
        public IQueryable<OrderListResponseItem> GetAll();
        public Task<BaseResultModel> Insert(Models.RequestOrder item);
    }
}
